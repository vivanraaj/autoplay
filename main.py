import textplayer.textPlayer as tP
import argparse
import agents.vivan_agent as vivan_agent
import sys
from keras import backend as K
from keras.layers import Dense, Flatten
from keras.models import Sequential
from keras_helper import NNWeightHelper
from snes import SNES
import pickle


## below for debugging
#sys.argv += 'zork1.z5'
#game_chosen = 'zork1.z5'



def manual_play(game_chosen):
	textPlayer = tP.TextPlayer(game_chosen)
	# pass the variable to another class
	state = textPlayer.run()
	print (state)
	action = raw_input('>> ')
	last_score = 0

	# we specify that exit is a way to quit game, so highlight obviously here
	while action!= 'exit':
		state = textPlayer.execute_command(action)
		print(state)
		if textPlayer.get_score() != None:
			score, __ = textPlayer.get_score()
			reward = score - last_score
			last_score = score
			print  ('Your overall score is {0} and you gained reward of {1} in the last action'.format(score,reward))
		action = raw_input('>> ')

	textPlayer.quit()

def train_agent(game_chosen, training_cycles = 1000):

	# start desired game file
	textPlayer = tP.TextPlayer(game_chosen)

	# defining the NN structure for neurovolui
	model = Sequential()
	model.add(Dense(100,input_shape=(100,)))
	model.add(Dense(100))
	model.compile(loss="mse", optimizer="adam")
	nnw = NNWeightHelper(model)
	weights = nnw.get_weights()


	track_scores = []
	counter = 0
	last_score = 0
	# defining population size
	pop_size = 1
	# defining generation size
	generations = 1
	# init the agent
	agent = vivan_agent.vivAgent()
	state = textPlayer.run()
	# print (state)
	# pass variables to SNES
	snes = SNES(weights, 1, pop_size)

	while (counter < training_cycles):
		for i in range(0, generations):		
			asked = snes.ask()	

			told = []
 
			j = 0 
			for asked_j in asked:
				# use SNES to set the weights of the NN
				# only run the SNES after 1st round
				if counter != 0:
					nnw.set_weights(asked_j)
					current_word2vec_vectors = agent.agent_scholar_vivan_get_word2vec_vectors()
					# take current vectors and use SNES to predict new vectors
					changed_word2vec_vectors = model.predict(current_word2vec_vectors)
					# send the new vectors back to the word2vec file
					agent.new_word2vec_vectors(changed_word2vec_vectors)						
				action = agent.take_action(state,False)
				state = textPlayer.execute_command(action)
				print('Counter no: {0} ::: {1} >>> {2}'.format(counter,action,state))
				print('This is asked No. {0} of generation no. {1}'.format(j,i))
				if textPlayer.get_score() != None:
					score, __ = textPlayer.get_score()
					reward = score - last_score
					told.append(reward)
					last_score = score
					agent.update(reward)
					accumulated_reward = agent.get_total_points_earned()
					print  ('Your overall score is {0} and you gained reward of {1} in the last action'.format(accumulated_reward,reward))
					track_scores.append((counter,state,j,i,action,reward,accumulated_reward))				
				j += 1
			snes.tell(asked,told)
		nnw.set_weights(snes.center)
		counter += 1
	
	# save the scores
	with open('track_scores.pkl', 'wb') as f:
		pickle.dump(track_scores, f)
	
	textPlayer.quit()


# for debugging
#if __name__ == '__main__':
#	manual_play(game_chosen)
#	train_agent(game_chosen)

parser = argparse.ArgumentParser(description='Which game do you want to play?')
parser.add_argument('file',help='Specify game name?')
function_map = {'manual_play' : manual_play,
                'train_agent' : train_agent }

parser.add_argument('command', choices=function_map.keys(),help = "Type manual_play if you want to manually play the game or " 
					" train_agent to train the agent")

# parse arguments
args = parser.parse_args()
game_chosen = args.file
func = function_map[args.command]
func(game_chosen)
# future: add if neuroevolution is on, specify training cycles, population, generation, plot tsne?